FROM eclipse-temurin:11-jre

COPY target/todolist-*.jar /app/todolist-service.jar

CMD ["java", "-jar", "/app/todolist-service.jar"]
