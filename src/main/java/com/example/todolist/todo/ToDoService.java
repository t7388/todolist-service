package com.example.todolist.todo;

import com.example.todolist.exception.ToDoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToDoService {

    private final ToDoRepository toDoRepository;

    @Autowired
    public ToDoService(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    public List<ToDo> findAllToDos() {
        return toDoRepository.findAll();
    }

    public ToDo findToDoById(Long id) {
        return toDoRepository
                .findById(id)
                .orElseThrow(
                        () -> new ToDoNotFoundException("ToDo with id " + id + " was not found")
                );

    }

    public ToDo addToDo(ToDo toDo) {
        return toDoRepository.save(toDo);
    }

    public ToDo updateToDo(ToDo toDo) {
        return toDoRepository.save(toDo);
    }

    public void deleteToDo(Long id) {
        toDoRepository.deleteById(id);
    }

}
