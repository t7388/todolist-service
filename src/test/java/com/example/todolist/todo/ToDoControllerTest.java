package com.example.todolist.todo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("unit-test")
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
class ToDoControllerTest {

    @Autowired
    ToDoRepository toDoRepository;

    @Autowired
    MockMvc mockMvc;

    @Test
    @Order(1)
    void findAllToDos() throws Exception {

        // when
        MvcResult result = mockMvc.perform(
                get("/todo/all")
        )
                .andDo(print())
                .andReturn();

        // then
        int responseCode = result.getResponse().getStatus();
        assertThat(responseCode).isEqualTo(200);

    }

    @Test
    @Order(2)
    void addToDo() throws Exception {

        // given
        ToDo toDo = createToDo();
        String toDoJson = toDoToJson(toDo);

        // when
        MvcResult result = mockMvc.perform(
                post("/todo/add")
                        .contentType(APPLICATION_JSON)
                        .content(toDoJson)
        )
                .andDo(print())
                .andReturn();

        // then
        int responseCode = result.getResponse().getStatus();
        assertThat(responseCode).isEqualTo(201);

        String responseJson = result.getResponse().getContentAsString();
        ToDo newTodo = jsonToToDo(responseJson);
        assertThat(newTodo.getId()).isInstanceOf(Long.class);
        assertThat(newTodo.getDescription()).isEqualTo(toDo.getDescription());
        assertThat(newTodo.getDueDate()).isEqualTo(toDo.getDueDate());
        assertThat(newTodo.getIsDone()).isEqualTo(toDo.getIsDone());

    }

    @Test
    @Order(3)
    void findToDoById() throws Exception {

        // given
        ToDo toDo = createToDo();

        // when
        MvcResult result = mockMvc.perform(
                get("/todo/1")
        )
                .andDo(print())
                .andReturn();

        // then
        int responseStatus = result.getResponse().getStatus();
        assertThat(responseStatus).isEqualTo(200);

        String toDoResponseJson = result.getResponse().getContentAsString();
        ToDo toDoResponse = jsonToToDo(toDoResponseJson);
        assertThat(toDoResponse.getId()).isInstanceOf(Long.class);
        assertThat(toDoResponse.getDescription()).isEqualTo(toDo.getDescription());
        assertThat(toDoResponse.getDueDate()).isEqualTo(toDo.getDueDate());
        assertThat(toDoResponse.getIsDone()).isEqualTo(toDo.getIsDone());

    }

    @Test
    @Order(4)
    void updateToDo() throws Exception {

        // given
        ToDo toDo = createToDo();
        toDo.setIsDone(true);

        String toDoJson = toDoToJson(toDo);

        // when
        MvcResult result = mockMvc.perform(
                put("/todo/update")
                        .contentType(APPLICATION_JSON)
                        .content(toDoJson)
        )
                .andDo(print())
                .andReturn();

        // then
        int responseStatus = result.getResponse().getStatus();
        assertThat(responseStatus).isEqualTo(200);

        String toDoResponseJson = result.getResponse().getContentAsString();
        ToDo toDoResponse = jsonToToDo(toDoResponseJson);
        assertThat(toDoResponse.getIsDone()).isEqualTo(true);

    }

    @Test
    @Order(5)
    void deleteToDo() throws Exception {

        // when
        MvcResult result = mockMvc.perform(
                delete("/todo/delete/1")
        )
                .andDo(print())
                .andReturn();

        // then
        int responseStatus = result.getResponse().getStatus();
        assertThat(responseStatus).isEqualTo(200);

        List<ToDo> toDos = toDoRepository.findAll();
        assertThat(toDos.size()).isEqualTo(0);

    }

    private ToDo createToDo() {

        ToDo toDo = new ToDo();
        toDo.setId(1L);
        toDo.setDescription("Example");
        toDo.setDueDate(LocalDate.now());
        toDo.setIsDone(false);

        return toDo;

    }

    private String toDoToJson(ToDo toDo) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(toDo);
    }

    private ToDo jsonToToDo(String json) throws JsonProcessingException {
        return new ObjectMapper().readValue(json, ToDo.class);
    }

}
